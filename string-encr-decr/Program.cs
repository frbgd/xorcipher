﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Linq;

namespace string_encr_decr
{
    public class XORCipher
    {
        //генератор повторений пароля
        private string GetRepeatKey(string s, int n)
        {
            var r = s;
            while (r.Length < n)
            {
                r += r;
            }

            return r.Substring(0, n);
        }

        //метод шифрования/дешифровки
        private string Cipher(string text, string secretKey)
        {
            var currentKey = GetRepeatKey(secretKey, text.Length);
            var res = string.Empty;
            for (var i = 0; i < text.Length; i++)
            {
                res += ((char)(text[i] ^ currentKey[i])).ToString();
            }

            return res;
        }

        //шифрование текста
        public string Encrypt(string plainText, string password)
            => Cipher(plainText, password);

        //расшифровка текста
        public string Decrypt(string encryptedText, string password)
            => Cipher(encryptedText, password);
    }
    class Program
    {
        static void Main(string[] args)
        {
            var x = new XORCipher();
            Console.Write("Введите текст сообщения: ");
            var message = Console.ReadLine();
            Console.Write("Введите пароль: ");
            var pass = Console.ReadLine();
            var encryptedMessageByPass = x.Encrypt(message, pass);
            Console.WriteLine("Зашифрованное сообщение :{0}:", encryptedMessageByPass);
            Console.WriteLine("Расшифрованное сообщение :{0}:", x.Decrypt(encryptedMessageByPass, pass));
            Console.ReadLine();
        }
    }
}
